package com.vend.pd.test.gfpf.pdvendgfpftest;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.GHEntityViewModel;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GHEntityUnitTest {
    GHEntityViewModel viewModel;

    @Test
    public void showValidRecyclerView(){
        // Trigger
        viewModel. getAll();


        // Validation
        Mockito.verify(viewModel.variable1).set("dummy text");
    }

}