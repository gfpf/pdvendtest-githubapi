package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface GHEntityDAO {

    @Insert
    void insert(GHEntity entity);

    @Update
    void update (GHEntity entity);

    @Delete
    void delete(GHEntity entity);

    @Query("DELETE FROM GHEntity_Table")
    void deleteAll();

    //@Query("SELECT * FROM GHEntity_Table ORDER BY priority_column DESC")
    /*@Query("SELECT * FROM GHEntity_Table")
    Single<List<GHEntity>> getAll();*/

}
