package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

public interface GHEntityRepository {

    Single<GHEntity> loadRepositoryByName(@NonNull String name, DisposableSingleObserver<GHEntity> callback);

    Single<List<GHIssuePull>> loadAllIssuesByRepository(
            @NonNull GHItem item
            , @NonNull DisposableSingleObserver<List<GHIssuePull>> callback);

    Single<List<GHIssuePull>> loadAllPullsByRepository(
            @NonNull GHItem item
            , @NonNull DisposableSingleObserver<List<GHIssuePull>> callback);


    void refreshData();
}
