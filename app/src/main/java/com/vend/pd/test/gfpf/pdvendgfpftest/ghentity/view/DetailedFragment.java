package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.view;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.vend.pd.test.gfpf.pdvendgfpftest.R;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.GHEntityContract;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailedFragment extends Fragment implements View.OnClickListener {

    private GHEntityContract.UserActionsListener mActionsListener;

    private GHIssuePull mIssuePull;

    @BindView(R.id.detailed_number)
    AppCompatTextView txtNumber;

    @BindView(R.id.detailed_title)
    AppCompatTextView txtTitle;

    @BindView(R.id.detailed_description)
    AppCompatTextView txtDescription;

    @BindView(R.id.view_specs)
    View viewSpecs;

    @BindView(R.id.btn_expand_specs)
    AppCompatButton btnExpandSpecs;

    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_detailed_frag, container, false);
        ButterKnife.bind(this, rootView);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.details);

        btnExpandSpecs.setOnClickListener(this);

        showRequestedItem();

        scrollView.fullScroll(View.FOCUS_UP);
        return rootView;
    }

    private void showRequestedItem() {
        Bundle bundle = getArguments();
        mIssuePull = (GHIssuePull) bundle.get(GHIssuePull.REQUESTED_ITEM_KEY);

        txtNumber.setText(String.valueOf(mIssuePull.getNumber()));
        txtTitle.setText(mIssuePull.getTitle());
        txtDescription.setText(mIssuePull.getDescription());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_expand_specs:
                if (viewSpecs.getVisibility() == View.GONE) {
                    viewSpecs.setVisibility(View.VISIBLE);
                    //scrollView.scrollTo(0, scrollView.getBottom());

                    //scroll();

                    //expand(viewSpecs);
                } else {
                    viewSpecs.setVisibility(View.GONE);
                    //collapse(viewSpecs);
                }
                break;
        }

    }

    public void scroll() {
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int scrollViewHeight = scrollView.getHeight();
                if (scrollViewHeight > 0) {

                    //TODO IMPLEMENT THE ELSE SCENARIO
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }

                    final View lastView = scrollView.getChildAt(scrollView.getChildCount() - 1);
                    final int lastViewBottom = lastView.getBottom() + scrollView.getPaddingBottom();
                    final int deltaScrollY = lastViewBottom - scrollViewHeight - scrollView.getScrollY();
                    /* If you want to see the scroll animation, call this. */
                    //scrollView.smoothScrollBy(0, deltaScrollY);
                    scrollView.smoothScrollTo(0, deltaScrollY);
                    /* If you don't want, call this. */
                    //scrollView.scrollBy(0, deltaScrollY);
                }
            }
        });

    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
