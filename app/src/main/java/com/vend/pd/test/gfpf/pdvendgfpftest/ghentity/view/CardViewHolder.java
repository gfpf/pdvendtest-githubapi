package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vend.pd.test.gfpf.pdvendgfpftest.R;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.view.CardRecyclerViewAdapter.RecyclerViewClickListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.ghentity_image)
    public ImageView image;

    @BindView(R.id.ghitem_name)
    public TextView username;

    @BindView(R.id.issue_or_pull)
    public TextView issueOrPull;

    @BindView(R.id.ghentity_name)
    public TextView name;

    @BindView(R.id.ghentity_full_name)
    public TextView fullName;
    private RecyclerViewClickListener mRecyclerViewClickListener;

    public CardViewHolder(@NonNull View itemView, RecyclerViewClickListener recyclerViewClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(this);
        mRecyclerViewClickListener = recyclerViewClickListener;
    }

    @Override
    public void onClick(View view) {
        mRecyclerViewClickListener.recyclerViewListClicked(view, getLayoutPosition());
    }
}
