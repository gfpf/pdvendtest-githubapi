package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain;

import com.google.gson.annotations.SerializedName;

public class GHItem {
    @SerializedName("id")
    private Integer mId;

    @SerializedName("node_id")
    private String mNodeId;

    @SerializedName("name")
    private String mName;

    @SerializedName("full_name")
    private String mFullName;

    @SerializedName("private")
    private Boolean mIsPrivate;

    @SerializedName("html_url")
    private String mHtmlUrl;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("issues_url")
    private String mIssuesUrl;

    @SerializedName("pulls_url")
    private String mPullsUrl;

    public GHItem(
            Integer mId
            , String mNodeId
            , String mName
            , String mFullName
            , Boolean mIsPrivate
            , String mHtmlUrl
            , String mDescription
            , String mIssuesUrl
            , String mPullsUrl) {
        
        this.mId = mId;
        this.mNodeId = mNodeId;
        this.mName = mName;
        this.mFullName = mFullName;
        this.mIsPrivate = mIsPrivate;
        this.mHtmlUrl = mHtmlUrl;
        this.mDescription = mDescription;
        this.mIssuesUrl = mIssuesUrl;
        this.mPullsUrl = mPullsUrl;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public Integer getId() {
        return mId;
    }

    public String getNodeId() {
        return mNodeId;
    }

    public String getName() {
        return mName;
    }

    public String getFullName() {
        return mFullName;
    }

    public Boolean getIsPrivate() {
        return mIsPrivate;
    }

    public void setIsPrivate(Boolean mIsPrivate) {
        this.mIsPrivate = mIsPrivate;
    }

    public String getHtlUrl() {
        return mHtmlUrl;
    }

    public void setHtlUrl(String mHtlUrl) {
        this.mHtmlUrl = mHtlUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getIssuesUrl() {
        return mIssuesUrl;
    }

    public void setIssuesUrl(String issuesUrl) {
        mIssuesUrl = issuesUrl;
    }

    public String getPullsUrl() {
        return mPullsUrl;
    }

    public void setPullsUrl(String pullsUrl) {
        mPullsUrl = pullsUrl;
    }



}
