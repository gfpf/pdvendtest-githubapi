package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Single;

public interface GHEntityContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showToastMessage(String message);

        void showGHIssuePullListUI(List<GHIssuePull> items, boolean isAppend);

        void showGHIssuePullDetailUI(@NonNull GHIssuePull requestedItem);
    }

    interface UserActionsListener {

        Single<GHEntity> searchRepositoryByName(String searchTerm, boolean forceUpdate);

        Single<List<GHIssuePull>> loadAllIssuesByRepository(@NonNull GHItem item);

        Single<List<GHIssuePull>> loadAllPullsByRepository(@NonNull GHItem item);

        void openGHIssuePullDetail(String id);
    }
}