package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain;

import java.util.List;

public class GHItems {

    private List<GHItem> mItems;

    public List<GHItem> getItems() {
        return mItems;
    }

    public void setItems(List<GHItem> items) {
        mItems = items;
    }

}
