package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.vend.pd.test.gfpf.pdvendgfpftest.MainActivity;
import com.vend.pd.test.gfpf.pdvendgfpftest.R;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.GHEntityContract;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.GHEntityViewModel;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;
import com.vend.pd.test.gfpf.pdvendgfpftest.utils.Util;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends Fragment implements
        GHEntityContract.View
        , CardRecyclerViewAdapter.RecyclerViewClickListener {

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.results_label)
    TextView resultsLabel;

    private GHEntityViewModel mGHEntityViewModel;
    private CardRecyclerViewAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_main_frag, container, false);
        ButterKnife.bind(this, rootView);
        setRetainInstance(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_header_details);

        // Set up the RecyclerView
        recyclerView.setHasFixedSize(true);
        //recyclerView.setItemAnimator();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        int smallPadding = getResources().getDimensionPixelSize(R.dimen.nav_header_vertical_spacing);
        CardItemDecoration itemDecoration = new CardItemDecoration(smallPadding, smallPadding);
        recyclerView.addItemDecoration(itemDecoration);

        //Adapter
        if (mAdapter == null) {
            mAdapter = new CardRecyclerViewAdapter(getContext(), this);
        } else {
            resultsLabel.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        recyclerView.setAdapter(mAdapter);


        int searchViewPlateId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);

        EditText searchPlateEditText = searchView.findViewById(searchViewPlateId);
        searchPlateEditText.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                String searchTerm = v.getText().toString();

                if (!TextUtils.isEmpty(searchTerm)) {
                    //Text entered
                    doSearch(searchTerm);

                } else {
                    //No search term
                    searchPlateEditText.setError(getString(R.string.required_field));
                }
            }
            return true;
        });


        return rootView;

    }

    private void doSearch(String searchTerm) {
        Util.hideKeyboard(getActivity());
        progressBar.setVisibility(View.VISIBLE);
        resultsLabel.setVisibility(View.GONE);


        //TODO Use observe method with LiveData only
        mGHEntityViewModel = ViewModelProviders.of(this).get(GHEntityViewModel.class);
        /*mGHEntityViewModel.searchRepositoryByName(searchTerm, false).observe(this, new Observer<List<GHIssuePull>>() {
            @Override
            public void onChanged(List<GHIssuePull> ghEntities) {
                //Update RecyclerView
                Toast.makeText(getActivity(), "onChanged", Toast.LENGTH_SHORT).show();
                mAdapter.replaceData(ghEntities);
            }

        });*/

        mGHEntityViewModel.searchRepositoryByName(searchTerm, false)
                .subscribe(ghEntity -> {
                    // handle data fetched successfully and API call completed
                    //Toast.makeText(getActivity(), ghEntity.toString(), Toast.LENGTH_SHORT).show();

                    //Result
                    if (ghEntity != null && ghEntity.getItems() != null) {

                        int firstItem = 0;
                        if (ghEntity.getItems().size() > firstItem) {

                            GHItem ghItem = ghEntity.getItems().get(firstItem);

                            //Get all issues by repository
                            mGHEntityViewModel.loadAllIssuesByRepository(ghItem)
                                    .subscribe(ghIssues -> {
                                        //Result
                                        showGHIssuePullListUI(ghIssues, false);

                                        //Get all pulls by repository
                                        mGHEntityViewModel.loadAllPullsByRepository(ghItem)
                                                .subscribe(ghPulls -> {
                                                    //Result
                                                    showGHIssuePullListUI(ghPulls, true);

                                                }, throwable -> {
                                                    // handle error event
                                                    showGHIssuePullListUI(null, true);
                                                });


                                    }, throwable -> {
                                        // handle error event
                                        showGHIssuePullListUI(null, true);
                                    });
                        }
                    }

                }, throwable -> {
                    // handle error event
                    showGHIssuePullListUI(null, true);
                });

    }


    @Override
    public void setProgressIndicator(boolean active) {

    }

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void showGHIssuePullListUI(List<GHIssuePull> items, boolean isAppend) {
        if (items == null || items.isEmpty()) {
            resultsLabel.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            resultsLabel.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            if (isAppend) {
                mAdapter.appendData(items);

            } else {
                mAdapter.replaceData(items);
            }
        }

        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        GHIssuePull selectedItem = mAdapter.getItem(position);
        showGHIssuePullDetailUI(selectedItem);
    }

    @Override
    public void showGHIssuePullDetailUI(GHIssuePull requestedItem) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(GHIssuePull.REQUESTED_ITEM_KEY, requestedItem);

        DetailedFragment detailedOrderFragment = new DetailedFragment();
        detailedOrderFragment.setArguments(bundle);

        MainActivity.class.cast(getActivity()).changeFragment(detailedOrderFragment, true, false);


    }
}
