package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;

import java.util.List;

import io.reactivex.Single;

public class GHEntityRepositoryClass {

    private GHEntityDAO mGHEntityDAO;

    private Single<List<GHEntity>> mAllGHEntities;


    public GHEntityRepositoryClass(Application application) {
        //Populate DB
        GHEntityDB database = GHEntityDB.getInstance(application);

        mGHEntityDAO = database.getGHEntityDAO();
        //mAllGHEntities = mGHEntityDAO.getAll();
    }

    public void insert(GHEntity entity) {
        new InsertGHEntityAsyncTask(mGHEntityDAO).execute(entity);
    }

    public void update(GHEntity ghEntity) {
        new UpdateGHEntityAsyncTask(mGHEntityDAO).execute(ghEntity);

    }

    public void delete(GHEntity ghEntity) {
        new DeleteGHEntityAsyncTask(mGHEntityDAO).execute(ghEntity);
    }

    public void deleteAll() {
        new DeleteAllGHEntitysAsyncTask(mGHEntityDAO).execute();

    }

    public Single<List<GHEntity>> getAll() {
        return mAllGHEntities;
    }

    /*public Single<GHEntity> getByName() {
        //TODO IMPLEMENT THIS SCENARIO
    }*/



    //TASKS

    private static class InsertGHEntityAsyncTask extends AsyncTask<GHEntity, Void, Void> {

        private GHEntityDAO mGHEntityDAO;

        private InsertGHEntityAsyncTask(GHEntityDAO ghEntityDAO) {
            mGHEntityDAO = ghEntityDAO;

        }

        @Override
        protected Void doInBackground(GHEntity... entities) {
            mGHEntityDAO.insert(entities[0]);

            return null;
        }
    }


    private static class UpdateGHEntityAsyncTask extends AsyncTask<GHEntity, Void, Void> {

        private GHEntityDAO mGHEntityDAO;

        private UpdateGHEntityAsyncTask(GHEntityDAO ghEntityDAO) {
            mGHEntityDAO = ghEntityDAO;

        }

        @Override
        protected Void doInBackground(GHEntity... ghEntitys) {
            mGHEntityDAO.update(ghEntitys[0]);

            return null;
        }
    }


    private static class DeleteGHEntityAsyncTask extends AsyncTask<GHEntity, Void, Void> {

        private GHEntityDAO mGHEntityDAO;

        private DeleteGHEntityAsyncTask(GHEntityDAO ghEntityDAO) {
            mGHEntityDAO = ghEntityDAO;

        }

        @Override
        protected Void doInBackground(GHEntity... ghEntitys) {
            mGHEntityDAO.delete(ghEntitys[0]);

            return null;
        }
    }


    private static class DeleteAllGHEntitysAsyncTask extends AsyncTask<Void, Void, Void> {

        private GHEntityDAO mGHEntityDAO;

        private DeleteAllGHEntitysAsyncTask(GHEntityDAO ghEntityDAO) {
            mGHEntityDAO = ghEntityDAO;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            mGHEntityDAO.deleteAll();

            return null;
        }
    }


}
