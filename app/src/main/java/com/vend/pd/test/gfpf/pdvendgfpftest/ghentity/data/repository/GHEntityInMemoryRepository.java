package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.ServiceApi;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class GHEntityInMemoryRepository implements GHEntityRepository {

    private final ServiceApi mServiceApi;

    /**
     * This method has reduced visibility for testing and is only visible to tests in the same package.
     */
    @VisibleForTesting
    private List<GHEntity> mCachedResults;

    public GHEntityInMemoryRepository(@NonNull ServiceApi serviceApi) {
        mServiceApi = checkNotNull(serviceApi);
    }


    @Override
    public Single<GHEntity> loadRepositoryByName(String name, DisposableSingleObserver<GHEntity> callback) {
        checkNotNull(name);

        //Search repository by name
        return mServiceApi.searchGHEntityByName(name, ServiceApi.STARS_SORT_KEY, ServiceApi.DESC_ORDER_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        //.subscribeWith(callback);
    }

    @Override
    public Single<List<GHIssuePull>> loadAllIssuesByRepository(GHItem item, DisposableSingleObserver<List<GHIssuePull>> callback) {
        String issuesUrl = ServiceApi.REPOS_URL_PATH_KEY + item.getFullName() + ServiceApi.ISSUES_URL_PATH_KEY;

        //Search repository by name
        return mServiceApi.fetchAllGHIssueByRepository(issuesUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        //.subscribeWith(callback);
    }

    @Override
    public Single<List<GHIssuePull>> loadAllPullsByRepository(GHItem item, DisposableSingleObserver<List<GHIssuePull>> callback) {
        String pullsUrl = ServiceApi.REPOS_URL_PATH_KEY + item.getFullName() + ServiceApi.PULLS_URL_PATH_KEY;

        //Search repository by name
        return mServiceApi.fetchAllGHPulls(pullsUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        //.subscribeWith(callback);
    }


    @Override
    public void refreshData() {

    }
}
