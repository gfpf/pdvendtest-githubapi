package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.BaseResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

@Entity(tableName = "GHEntity_Table")
public class GHEntity extends BaseResponse {

    @PrimaryKey(autoGenerate = true)
    private Integer mId;

    @SerializedName("total_count")
    private Integer mTotalCount;

    @SerializedName("incomplete_results")
    private boolean mIncompleteResults;

    @TypeConverters(GHEntityConverter.class)
    @SerializedName("items")
    private ArrayList<GHItem> mItems;

    /*@ColumnInfo(name = "priority_column")
    private int mPriority;*/

    public GHEntity() {

    }

    @Ignore
    public GHEntity(Integer mTotalCount, boolean mIncompleteResults) {
        this.mTotalCount = mTotalCount;
        this.mIncompleteResults = mIncompleteResults;
    }

    @Ignore
    public GHEntity(Integer totalCount, boolean incompleteResults, ArrayList<GHItem> items) {
        mTotalCount = totalCount;
        mIncompleteResults = incompleteResults;
        mItems = items;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public Integer getTotalCount() {
        return mTotalCount;
    }

    public void setTotalCount(Integer totalCount) {
        mTotalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return mIncompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        mIncompleteResults = incompleteResults;
    }

    public ArrayList<GHItem> getItems() {
        return mItems;
    }

    public void setItems(ArrayList<GHItem> items) {
        mItems = items;
    }

    /*public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }*/

    public static class GHEntityConverter {

        @TypeConverter
        public static ArrayList<GHItem> storedStringToMyObjects(String data) {
            Gson gson = new Gson();

            if (data == null) {
                return new ArrayList<>(Collections.emptyList());
            }
            Type ArrayListType = new TypeToken<ArrayList<GHItem>>() {
            }.getType();
            return gson.fromJson(data, ArrayListType);
        }

        @TypeConverter
        public static String myObjectsToStoredString(ArrayList<GHItem> myObjects) {
            Gson gson = new Gson();
            return gson.toJson(myObjects);
        }
    }

}
