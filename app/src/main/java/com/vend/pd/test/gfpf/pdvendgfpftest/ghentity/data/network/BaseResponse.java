package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network;

public class BaseResponse {

    private String mError;

    public String getError() {
        return mError;
    }

    public void setError(String error) {
        mError = error;
    }

}
