package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ServiceApi {

    public static final String STARS_SORT_KEY = "stars";
    public static final String DESC_ORDER_KEY = "desc";

    public static final String REPOS_URL_PATH_KEY = "repos/";
    public static final String ISSUES_URL_PATH_KEY = "/issues";
    public static final String PULLS_URL_PATH_KEY = "/pulls";

    // Create GHEntity
    @FormUrlEncoded
    @POST("repositories/new")
    void createGHEntity(@Field("GHEntity") GHEntity... entities);

    // Create GHItem
    void createGHItem(GHItem... items);

    // Create Issue
    @FormUrlEncoded
    @POST("repos/{full_name}/issues/new")
    void createGHIssuePull(@Field("GHIssuePull") GHIssuePull... entities);

    // Fetch all GHEntities
    @GET("repositories")
    Single<List<GHEntity>> fetchAllGHEntities();

    // Fetch all GHEntities
    @GET("search/repositories")
    Single<GHEntity> searchGHEntityByName(
            @Query("q") String name
            , @Query("sort") String sort
            , @Query("order") String order);


    //@GET("repos/{full_name}/issues")
    @GET
    Single fetchAllGHIssueByRepository(@Url String url);


    @GET
    Single<List<GHIssuePull>> fetchAllGHPulls(@Url String url);


    // Update single GHEntity
    @FormUrlEncoded
    @PUT("repositories/{id}")
    Completable updateGHEntity(
            @Path("id") int id
            , @Field("GHEntity") String ghEntity);

    // Delete GHEntity
    @DELETE("repositories/{id}")
    Completable deleteGHEntity(@Path("id") int id);
}
