package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.ServiceApi;

import androidx.annotation.NonNull;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class Repositories {

    private Repositories() {
        // no instance
    }

    private static GHEntityRepository repository = null;

    public synchronized static GHEntityRepository getGHEntityInMemoryRepository(@NonNull ServiceApi serviceApi) {
        checkNotNull(serviceApi);

        if (repository == null) {
            repository = new GHEntityInMemoryRepository(serviceApi);
        }
        return repository;
    }

}
