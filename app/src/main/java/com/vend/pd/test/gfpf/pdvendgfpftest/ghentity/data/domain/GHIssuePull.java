package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GHIssuePull implements Serializable {

    public static final String REQUESTED_ITEM_KEY = "REQUESTED_ITEM_KEY";

    @SerializedName("id")
    private Integer mId;

    @SerializedName("number")
    private Integer mNumber;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("body")
    private String mDescription;

    @SerializedName("user")
    private GHUser mUser;

    @SerializedName("repository_url")
    private String mRepositoryUrl;

    private boolean mIsIssue;

    public GHIssuePull(Integer id, Integer number, String title, GHUser user, String repositoryUrl) {
        mId = id;
        mNumber = number;
        mTitle = title;
        mUser = user;
        mRepositoryUrl = repositoryUrl;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public GHUser getUser() {
        return mUser;
    }

    public void setUser(GHUser user) {
        mUser = user;
    }

    public Integer getNumber() {
        return mNumber;
    }

    public void setNumber(Integer number) {
        mNumber = number;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean isIssue() {
        return (getRepositoryUrl() != null);
    }

    public String getRepositoryUrl() {
        return mRepositoryUrl;
    }

    public void setRepositoryUrl(String repositoryUrl) {
        mRepositoryUrl = repositoryUrl;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}
