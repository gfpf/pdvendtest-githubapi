package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHUser;

import java.util.ArrayList;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {GHEntity.class}, version = 1, exportSchema = false)
public abstract class GHEntityDB extends RoomDatabase {

    private static GHEntityDB instance;

    public abstract GHEntityDAO getGHEntityDAO();

    public static synchronized GHEntityDB getInstance(Context context) {
        if (instance == null) {

            instance = Room.databaseBuilder(context.getApplicationContext()
                    , GHEntityDB.class, "gh_repo_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();

        }

        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(SupportSQLiteDatabase db) {
            super.onCreate(db);

            new PopulateDBAsyncTask(instance).execute();

        }
    };

    private static class PopulateDBAsyncTask extends AsyncTask<Void, Void, Void> {

        private GHEntityDAO mGHEntityDAO;

        public PopulateDBAsyncTask(GHEntityDB ghEntityDB) {
            mGHEntityDAO = ghEntityDB.getGHEntityDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ArrayList list = new ArrayList<GHIssuePull>();

            GHIssuePull issue = new GHIssuePull(1, 123, "Title 1", new GHUser(), null);
            list.add(issue);

            issue = new GHIssuePull(2, 123, "Title 2", new GHUser(), null);
            list.add(issue);

            issue = new GHIssuePull(3, 123, "Title 3", new GHUser(), "");
            list.add(issue);


            mGHEntityDAO.insert(new GHEntity(1, false, list));

            return null;
        }
    }


}
