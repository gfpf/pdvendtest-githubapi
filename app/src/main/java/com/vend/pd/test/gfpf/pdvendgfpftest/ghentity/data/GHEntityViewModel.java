package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data;

import android.app.Application;

import com.vend.pd.test.gfpf.pdvendgfpftest.Injection;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository.GHEntityRepository;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import io.reactivex.Single;


public class GHEntityViewModel extends AndroidViewModel implements GHEntityContract.UserActionsListener {

    private Application mApplication;
    private GHEntityRepository mghEntityRepository;

    public GHEntityViewModel(Application application) {
        super(application);
        mApplication = application;
        mghEntityRepository = Injection.provideGHEntityRepository(application);
    }

    @Override
    public Single<GHEntity> searchRepositoryByName(String searchTerm, boolean forceUpdate) {

        if (forceUpdate) {
            mghEntityRepository.refreshData();
        }

        return mghEntityRepository.loadRepositoryByName(searchTerm, null);

        //Get repository by name
        /*return mghEntityRepository.loadRepositoryByName(searchTerm, new DisposableSingleObserver<GHEntity>() {

            @Override
            public void onSuccess(GHEntity ghEntity) {

                // Received entity
                if (ghEntity != null && ghEntity.getItems() != null) {

                    int firstItem = 0;
                    if (ghEntity.getItems().size() > firstItem) {

                        GHItem ghItem = ghEntity.getItems().get(firstItem);


                        //Get all issues by repository
                        mghEntityRepository.loadAllIssuesByRepository(ghItem, new DisposableSingleObserver<List<GHIssuePull>>() {

                            @Override
                            public void onSuccess(List<GHIssuePull> ghIssuePulls) {
                                //Received all repositories
                                ghIssuePullList.addAll(ghIssuePulls);


                                //Get all pulls by repository
                                mghEntityRepository.loadAllPullsByRepository(ghItem, new DisposableSingleObserver<List<GHIssuePull>>() {

                                    @Override
                                    public void onSuccess(List<GHIssuePull> ghIssuePulls) {
                                        ghIssuePullList.addAll(ghIssuePulls);

                                        //TODO SHOW RESULTS
                                        //mGHEntityView.showGHIssuePullListUI(ghIssuePullList);
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }
                                });
                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        });


                    } else {
                        //mGHEntityView.showGHIssuePullListUI(null);
                        //showResults(null, false);
                    }

                } else {
                    //mGHEntityView.showGHIssuePullListUI(null);
                    //showResults(null, false);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

        });*/

        //return ghIssuePullList;

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        //EspressoIdlingResource.increment(); // App is busy until further notice

        //Get all socialPost items
        /*mghEntityRepository.loadghEntity(new GHEntityRepository.ghEntityRepositoryCallback() {
            @Override
            public void onghEntityLoaded(Single<List<GHIssuePull>> items) {
                //EspressoIdlingResource.decrement(); //Set app as idle.
                mGHEntityView.setProgressIndicator(false);
                mGHEntityView.showGHIssuePullListUI(items);

            }

            @Override
            public void onghEntityCanceled() {
                //EspressoIdlingResource.decrement(); // Set app as idle.
                mGHEntityView.setProgressIndicator(false);
                mGHEntityView.showToastMessage(mApplication.getString(R.string.connection_error));
            }


        });*/

    }

    @Override
    public Single<List<GHIssuePull>> loadAllIssuesByRepository(GHItem item) {
        return mghEntityRepository.loadAllIssuesByRepository(item, null);
    }

    @Override
    public Single<List<GHIssuePull>> loadAllPullsByRepository(GHItem item) {
        return mghEntityRepository.loadAllPullsByRepository(item, null);
    }

    /*private void getIssuesByRepository(final GHItem ghItem) {
        mghEntityRepository.loadAllIssuesByRepository(ghItem, new DisposableSingleObserver<List<GHIssuePull>>() {

            @Override
            public void onSuccess(List<GHIssuePull> ghIssuePulls) {
                //Received all repositories
                ghIssuePullList.addAll(ghIssuePulls);


            }

            @Override
            public void onError(Throwable e) {

            }
        });



        *//*String issuesUrl = ServiceApi.REPOS_URL_PATH_KEY + ghItem.getFullName() + ServiceApi.ISSUES_URL_PATH_KEY;

        apiService.fetchAllGHIssueByRepository(issuesUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<GHIssuePull>>() {
                    @Override
                    public void onSuccess(List<GHIssuePull> ghIssues) {
                        // Received all issues
                        showResults(ghIssues, false);
                        getPullsByRepository(apiService, ghItem);

                    }

                    @Override
                    public void onError(Throwable e) {
                        // Network error
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
                    }
                });*//*

    }

    private void getPullsByRepository(ServiceApi apiService, GHItem ghItem) {
        String pullsUrl = ServiceApi.REPOS_URL_PATH_KEY + ghItem.getFullName() + ServiceApi.PULLS_URL_PATH_KEY;

        apiService.fetchAllGHPulls(pullsUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<GHIssuePull>>() {
                    @Override
                    public void onSuccess(List<GHIssuePull> ghIssues) {
                        // Received all pulls
                        showResults(ghIssues, true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Network error
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
                    }
                });

    }*/

    private void showResults(List<GHIssuePull> items, boolean isAppend) {
        /*if (items == null || items.isEmpty()) {
            resultsLabel.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            resultsLabel.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            if (isAppend) {
                mAdapter.appendData(items);

            } else {
                mAdapter.replaceData(items);
            }
        }

        progressBar.setVisibility(View.GONE);*/
    }

    @Override
    public void openGHIssuePullDetail(String id) {

    }
}