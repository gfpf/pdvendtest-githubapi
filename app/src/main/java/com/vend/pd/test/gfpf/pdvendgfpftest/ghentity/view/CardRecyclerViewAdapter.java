package com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.vend.pd.test.gfpf.pdvendgfpftest.R;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.core.util.Preconditions.checkNotNull;

public class CardRecyclerViewAdapter extends RecyclerView.Adapter<CardViewHolder> {

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }

    private Context mContext;
    private List<GHIssuePull> mGHIssues = new ArrayList<>();
    private RecyclerViewClickListener mRecyclerViewClickListener;

    public CardRecyclerViewAdapter(Context context, RecyclerViewClickListener recyclerViewClickListener) {
        mContext = context;
        mRecyclerViewClickListener = recyclerViewClickListener;
    }


    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new CardViewHolder(layoutView, mRecyclerViewClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        if (mGHIssues != null && position < mGHIssues.size()) {

            GHIssuePull ghIssue = mGHIssues.get(position);

            //Is Issue
            if (ghIssue.isIssue()) {
                holder.issueOrPull.setText(mContext.getString(R.string.issue));
                holder.issueOrPull.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_dark));

            } else {
                holder.issueOrPull.setText(mContext.getString(R.string.pull));
                holder.issueOrPull.setTextColor(mContext.getResources().getColor(android.R.color.holo_blue_dark));
            }

            //Entity Name
            holder.name.setText(String.valueOf(ghIssue.getNumber()));

            //Entity Full Name
            holder.fullName.setText(ghIssue.getTitle());

            //User image
            if (ghIssue.getUser() != null) {
                Picasso.with(mContext)
                        .load(ghIssue.getUser().getAvatarUrl())
                        .placeholder(R.drawable.ic_thumbnail)
                        //.resize(150, 150)
                        //.centerCrop()
                        .into(holder.image);

                //User name
                holder.username.setText(ghIssue.getUser().getLogin());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mGHIssues.size();
    }

    public GHIssuePull getItem(int position) {
        return mGHIssues.get(position);
    }

    public void replaceData(List<GHIssuePull> ghIssues) {
        setList(ghIssues);
        notifyDataSetChanged();
    }

    public void appendData(List<GHIssuePull> ghIssues) {
        this.mGHIssues.addAll(ghIssues);
        notifyDataSetChanged();
    }

    private void setList(List<GHIssuePull> ghIssues) {
        this.mGHIssues = checkNotNull(ghIssues);
    }
}
