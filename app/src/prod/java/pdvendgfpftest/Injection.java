package com.vend.pd.test.gfpf.pdvendgfpftest;

import android.content.Context;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.RetrofitServiceApiClient;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.ServiceApi;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository.GHEntityRepository;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository.Repositories;


public class Injection {

    public static GHEntityRepository provideGHEntityRepository(Context context) {
        ServiceApi serviceApi = RetrofitServiceApiClient.getClient(context)
                .create(ServiceApi.class);

        return Repositories.getGHEntityInMemoryRepository(serviceApi);
    }

}
