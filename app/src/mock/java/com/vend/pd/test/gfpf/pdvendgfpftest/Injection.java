package com.vend.pd.test.gfpf.pdvendgfpftest;

import android.content.Context;

import com.vend.pd.test.gfpf.pdvendgfpftest.data.FakeServiceApiImpl;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository.GHEntityRepository;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.repository.Repositories;


public class Injection {

    public static GHEntityRepository provideGHEntityRepository(Context context) {
        return Repositories.getGHEntityInMemoryRepository(new FakeServiceApiImpl());
    }

}
