package com.vend.pd.test.gfpf.pdvendgfpftest.data;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHUser;

import java.util.ArrayList;

import androidx.collection.ArrayMap;

@SuppressWarnings("unchecked")
public class FakeServiceApiEndpoint {

    private final static ArrayMap FAKE_GH_ISSUE_PULL_SERVICE_DATA;

    static {
        FAKE_GH_ISSUE_PULL_SERVICE_DATA = new ArrayMap(10);
        addGHIssuePull(1, 1, "Fake entity 1", null, null);
        addGHIssuePull(2, 2, "Fake entity 2", null, null);
        addGHIssuePull(3, 3, "Fake entity 3", null, null);
        addGHIssuePull(4, 4, "Fake entity 4", null, null);
        addGHIssuePull(5, 5, "Fake entity 5", null, null);
        addGHIssuePull(6, 6, "Fake entity 6", null, "");
        addGHIssuePull(7, 7, "Fake entity 7", null, "");
        addGHIssuePull(8, 8, "Fake entity 8", null, "");
        addGHIssuePull(9, 9, "Fake entity 9", null, "");
        addGHIssuePull(0, 0, "Fake entity 0", null, "");
    }


    private final static ArrayMap FAKE_GH_ITEM_SERVICE_DATA;

    static {
        FAKE_GH_ITEM_SERVICE_DATA = new ArrayMap(1);
        addGHItem(
                1
                , "MDEwOlJlcG9zaXRvcnk3NTA4NDEx"
                , "RxJava"
                , "ReactiveX/RxJava"
                , false
                , "https://github.com/ReactiveX/RxJava"
                , "RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM."
                , "https://api.github.com/repos/ReactiveX/RxJava/issues{/number}"
                , "https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}");
    }


    private final static ArrayMap FAKE_GH_ENTITY_SERVICE_DATA;

    static {
        FAKE_GH_ENTITY_SERVICE_DATA = new ArrayMap(1);
        addGHEntity(1234, false, new ArrayList<>(FAKE_GH_ITEM_SERVICE_DATA.values()));
    }


    private static void addGHIssuePull(int id, int number, String title, GHUser user, String repositoryUrl) {
        GHIssuePull ghEntity = new GHIssuePull(id, number, title, user, repositoryUrl);
        FAKE_GH_ISSUE_PULL_SERVICE_DATA.put(ghEntity.getId(), ghEntity);
    }


    private static void addGHItem(
            int id
            , String nodeId
            , String name
            , String fullName
            , Boolean isPrivate
            , String htmlUrl
            , String description
            , String issuesUrl
            , String pullsUrl) {

        GHItem item = new GHItem(id, nodeId, name, fullName, isPrivate, htmlUrl, description, issuesUrl, pullsUrl);
        FAKE_GH_ITEM_SERVICE_DATA.put(item.getId(), item);
    }


    private static void addGHEntity(int totalCount, boolean incompleteResults, ArrayList<GHItem> items) {
        GHEntity ghEntity = new GHEntity(totalCount, incompleteResults, items);
        FAKE_GH_ENTITY_SERVICE_DATA.put(ghEntity.getId(), ghEntity);
    }


    public static ArrayMap<Integer, GHIssuePull> loadIssuesPullsPersisted() {
        return FAKE_GH_ISSUE_PULL_SERVICE_DATA;
    }

    public static ArrayMap<Integer, GHItem> loadGHItemsPersisted() {
        return FAKE_GH_ITEM_SERVICE_DATA;
    }

    public static ArrayMap<Integer, GHEntity> loadGHEntitiesPersisted() {
        return FAKE_GH_ENTITY_SERVICE_DATA;
    }
}
