package com.vend.pd.test.gfpf.pdvendgfpftest.data;

import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHEntity;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHIssuePull;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.domain.GHItem;
import com.vend.pd.test.gfpf.pdvendgfpftest.ghentity.data.network.ServiceApi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import androidx.annotation.VisibleForTesting;
import androidx.collection.ArrayMap;
import io.reactivex.Completable;
import io.reactivex.Single;

public class FakeServiceApiImpl implements ServiceApi {

    private static final ArrayMap<Integer, GHIssuePull> GH_ISSUE_PULL_SERVICE_DATA = FakeServiceApiEndpoint.loadIssuesPullsPersisted();

    private static final ArrayMap<Integer, GHItem> GH_ITEM_SERVICE_DATA = FakeServiceApiEndpoint.loadGHItemsPersisted();

    private static final ArrayMap<Integer, GHEntity> GH_ENTITY_SERVICE_DATA = FakeServiceApiEndpoint.loadGHEntitiesPersisted();


    @Override
    @VisibleForTesting
    public void createGHEntity(GHEntity... entities) {
        for (GHEntity ghEntity : entities) {
            GH_ENTITY_SERVICE_DATA.put(ghEntity.getId(), ghEntity);
        }
    }

    @Override
    public void createGHIssuePull(GHIssuePull... entities) {
        for (GHIssuePull ghIssuePull : entities) {
            GH_ISSUE_PULL_SERVICE_DATA.put(ghIssuePull.getId(), ghIssuePull);
        }
    }

    @Override
    public void createGHItem(GHItem... entities) {
        for (GHItem ghItem : entities) {
            GH_ITEM_SERVICE_DATA.put(ghItem.getId(), ghItem);
        }
    }


    @Override
    public Single<List<GHEntity>> fetchAllGHEntities() {
        //return Lists.newArrayList(GH_ISSUE_PULL_SERVICE_DATA.values();
        return null;
    }

    @Override
    public Single<GHEntity> searchGHEntityByName(String searchTerm, String sort, String order) {

        for (Map.Entry entry : GH_ENTITY_SERVICE_DATA.entrySet()) {
            GHEntity entity = (GHEntity) entry.getValue();

            int firstItem = 0;
            GHItem ghItem = entity.getItems().get(firstItem);
            String itemName = ghItem.getName().trim().toLowerCase();
            searchTerm = searchTerm.trim().toLowerCase();

            if (itemName.equals(searchTerm) || itemName.contains(searchTerm)) {
                return Single.just(entity);
            }
        }

        return Single.error(new Throwable());
    }

    @Override
    public Single<List<GHIssuePull>> fetchAllGHIssueByRepository(String url) {
        Collection<GHIssuePull> values = GH_ISSUE_PULL_SERVICE_DATA.values();
        List<GHIssuePull> list = new ArrayList<>(values);
        return Single.just(list);
    }

    @Override
    public Single<List<GHIssuePull>> fetchAllGHPulls(String url) {
        Collection<GHIssuePull> values = GH_ISSUE_PULL_SERVICE_DATA.values();
        List<GHIssuePull> list = new ArrayList<>(values);
        return Single.just(list);
    }

    @Override
    public Completable updateGHEntity(int id, String ghEntity) {
        return null;
    }

    @Override
    public Completable deleteGHEntity(int id) {
        return null;
    }
}
